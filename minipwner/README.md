# MiniPwner
MiniPwner - penetration testing dropbox which can provide some interesting capabilities if you're able to insert one into a target's network. 

Can be use as a packet sniffer, hotspot or openvpn dropbox.

I not author, just make some fixes.

All credit goes to Kevin Bong & Michael Vieau for the initial project idea.

## Install
### 1. Prepare router for Minipwner installing http://telegra.ph/Kastom-cherez-bol-07-17
### 2. Download and extract minipwner-overlay  
    cd /tmp  
    wget https://gitlab.com/the29a/MR3040-tools/-/archive/master/MR3040-tools-master.tar --no-check-certificate  
    tar -xvf MR3040-tools-master.tar   
### 3. Run install script 
    sh MR3040-tools-master/minipwner/minipwner-overlay/setup.sh
### 4. Reboot router
### 5. Two ways of post-install  
#### a. Run post-install  
    cd /tmp  
    wget https://gitlab.com/the29a/MR3040-tools/raw/master/minipwner/post-install.sh --no-check-certificate  
    sh post-install.sh 
#### b. Install manually  
```
opkg update
opkg install libpcap libstdcpp libpthread zlib libopenssl libbz2 bzip2 terminfo libnet-1.2.x libevent2-pthreads 
opkg install libpcre libltdl libncurses librt libruby wireless-tools hostapd-common aircrack-ng libevent2-openssl 
opkg install samba36-server snort tar tcpdump tmux wget python vim unzip hostapd-utils libevent2 libevent2-extra 
opkg install ruby uclibcxx libnl libcap libreadline libdnet libdaq libuuid libffi openssl-util kmod-tun liblzo libevent2-core  
opkg install kismet-client kismet-drone kismet-server netcat nmap openvpn-easy-rsa openvpn-openssl perl samba36-client
```

## Notes    
Some packages are missing and some minimodes may not work.  
    Missing: libnet1 hostapd-common-old kmod-madwifi elinks ettercap karma yafc 