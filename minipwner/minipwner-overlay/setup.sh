#!/bin/sh
# Name:         setup.sh
# Purpose:      Install the MiniPwner overlay to OpenWrt or LEDE
# Author:       Michael Vieau
# Updated by:   the29a
# Date:         12.12.14
# Modified      17.07.18
# -----------------------------------------------
clear

# Vars
# ------
FDIR="files"

# Setup MiniPwner Dir
tar -xf minipwner.tar -C /

# Setup minimodes
mv -f $FDIR/minimodes /etc/init.d/minimodes
chmod +x /etc/init.d/minimodes
ln -s /etc/init.d/minimodes /etc/rc.d/S99minimodes
chmod +x /etc/rc.d/S99minimodes

# Move files as needed
mv -f $FDIR/minimodes.html /www/minimodes.html
chmod +x /www/minimodes.html
mv -f $FDIR/firewall /etc/config/firewall
chmod 644 /etc/config/firewall
mv -f $FDIR/banner /etc/banner
chmod 644 /etc/banner

# Clean up
rm -f minipwner.tar
rm -rf $FDIR
rm -f setup.sh

echo "The MiniPwner Overlay has been applied."
echo "Reboot your device for everything to take effect."
echo "---------------------------------------------------"
