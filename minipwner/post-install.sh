#!/bin/sh
# Name:     setup.sh
# Purpose:  Post-install script for MiniPwner
# By:       the29a
# Date:     17.07.18
# -----------------------------------------------
clear

#Install
opkg update
opkg install libpcap libstdcpp libpthread zlib libopenssl libbz2 bzip2 terminfo libnet-1.2.x libevent2-pthreads 
opkg install libpcre libltdl libncurses librt libruby wireless-tools hostapd-common aircrack-ng libevent2-openssl 
opkg install samba36-server snort tar tcpdump tmux wget python vim unzip hostapd-utils libevent2 libevent2-extra 
opkg install ruby uclibcxx libnl libcap libreadline libdnet libdaq libuuid libffi openssl-util kmod-tun liblzo libevent2-core  
opkg install kismet-client kismet-drone kismet-server netcat nmap openvpn-easy-rsa openvpn-openssl perl samba36-client

#Clean-up
rm -f post-install.sh
echo "Post-install complete."
echo "Reboot your device to take effect."
echo "---------------------------------------------------"